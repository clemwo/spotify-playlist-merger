# Spotify Playlist Merger

![](https://i.imgur.com/ijHsdSj.png)

Warning: Use this at your own risk. I highly recommend doing backups of your spotify library before using this tool. For backups check out [spotmybackup.com](http://www.spotmybackup.com/)

## Introduction

The Spotify Playlist Merger is a Python command line interface to merge Spotify playlists. Songs from one or multiple
given source playlists will be added to the specified target playlist.

## Setup

1. Install with ```pip3 install .```
2. Create a file called ``config.yaml`` with the content structured like this:

```yaml
---
settings:
  client_id: <your_client_id>
  client_secret: <your_client_secret>
alias:
  Rock - Complete: https://open.spotify.com/playlist/1M5BkYfc83RC1Ih0RNA9vs?si=bfd396c6dc254934
  Rock - Indie Rock: https://open.spotify.com/playlist/5JEqzvetrtmMQhZQZQYSfE?si=53e3df1958d44677
  Rock - Heavy Metal: https://open.spotify.com/playlist/3pWc9Abud5lj65SC1zGA8T?si=211b50956ee6467e
  Hip Hop - Complete: https://open.spotify.com/playlist/0OKqezw9yzS6IY4ypHynXR?si=b6cd9761812c4517
  Hip Hop - 90s: https://open.spotify.com/playlist/3nHgmhoxiCFPUvQrtLEB6a?si=d04618a3a81549d0
  Hip Hop - Jazz: https://open.spotify.com/playlist/2SPVbI5oOoHbj9gTFELaUM?si=5c3b66e85f9a403f
merge:
  Rock - Complete:
    - Rock - Indie Rock
    - Rock - Heavy Metal
  Hip Hop - Complete:
    - Hip Hop - 90s
    - Hip Hop - Jazz
```

### settings

- Get your own Spotify Client ID and Client Secret as
  explained [here](https://developer.spotify.com/documentation/general/guides/app-settings/) and put them in the yaml
  file
- Choose a path for logfiles
- Redirect URI can be any [valid URI](https://spotipy.readthedocs.io/en/2.19.0/#redirect-uri)

### alias

In this section of the config the track aliases get defined. An alias is an assignment of a Spotify Playlist Link to a
readable name. In the merge section only these aliases can be used to define which playlists to merge in order to make
mistakes less likely. Remember, this tool can't undo anything, so please be careful and do regular backups.

The creation of aliases can be done manually or automatically. Manually, just rightclick a playlist in the Spotify
Application, choose "Share", copy the playlist link and insert it into the alias section with a useful name. spotifypm
can also generate these aliases, see section "Usage".

### merge

The playlists that songs will be merged into are set as keys in "merge". The associated value of each key playlist are
lists of the playlists that will be merged into the specific key playlist.

```yaml
merge:
  Target Playlist 1:
    - Source Playlist 1.1
    - Source Playlist 1.2
  Target Playlist 2:
    - Source Playlist 2.1
    - Source Playlist 2.2
```

So in this setup, any song from ``Source Playlist 1.1`` and ``Source Playlist 1.2`` that isn't already
in ``Target Playlist 1`` will be merged into ``Target Playlist 1``.

## Usage

``spotifypm merge --help`` for more information

### Generate aliases

Create a config.yaml file in a location of your choice. The config has to have at least the "settings" section as seen
in [Setup](##Setup).

Execute ``spotifypm alias -p "path/to/config.yaml"`` in the commandline. If ``spotifypm alias`` gets executed without
the path option, the program will look in the default location. See console output for more information

``spotifypm alias --help`` for more information

### Merge playlists

Execute ```spotifypm merge -p "path/to/config.yaml"``` in the commandline. If ``spotifypm merge`` gets executed without
the path option, the program will look in the default location. See console output for more information

