import setuptools
from setuptools import setup

setup(
    name='spotifypm',
    version='0.1.0',
    packages=setuptools.find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'loguru',
        'spotipy',
        'PyYAML',
        'platformdirs',
        'cerberus'
    ],
    entry_points={
        'console_scripts': [
            'spotifypm = src.spotifypm:cli',
        ],
    },
)