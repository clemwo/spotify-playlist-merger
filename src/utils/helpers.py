import spotipy
import yaml
from pathlib import Path


def spotipy_authenticate(config):
    """
    Authenticates with spotify and returns an authenticated Spotipy Object
    """
    auth_manager = spotipy.oauth2.SpotifyOAuth(scope="playlist-modify-public playlist-modify-private "
                                                     "playlist-read-private",
                                               client_id=config['settings']['client_id'],
                                               client_secret=config['settings']['client_secret'],
                                               redirect_uri='http://localhost:8888/callback',
                                               open_browser=False)

    spotipy_client = spotipy.Spotify(auth_manager=auth_manager)
    # quick check to see if authentication works, currently just times out
    spotipy_client.me()
    return spotipy_client


def get_all_user_playlists(sp, user_id):
    """
    Returns all the playlists that the user owns
    """
    # Slightly confusing: playlists_dict is a dict that has a key 'items' that holds the playlists themselves
    playlists_dict = sp.user_playlists(user_id)
    playlist_items = extend_user_playlists(sp, playlists_dict)
    return playlist_items


def extend_user_playlists(sp, playlists_dict):
    """
    Spotify API returns a maximum of 50 playlists per user.
    This function extends the list of playlists with multiple API calls in case there are more than 50 playlists
    """
    playlists_items = playlists_dict['items']
    while playlists_dict['next']:
        # save the additional playlists in next_playlists
        next_playlists = sp.next(playlists_dict)
        playlists_items.extend(next_playlists['items'])
        playlists_dict = next_playlists
    return playlists_items


def extend_playlist_tracks(sp, playlist):
    """
    Spotify API returns a maximum of 100 tracks for a given playlist.
    This function calls the API multiple times and extends playlists longer than 100 tracks
    """
    playlist_tracks = playlist['tracks']
    while playlist_tracks['next']:
        next_tracks = sp.next(playlist_tracks)
        playlist['tracks']['items'].extend(next_tracks['items'])
        playlist_tracks = next_tracks


def divide_chunks(list_in, chunk_len):
    """
    Given a list longer than n, returns a list of lists with each sublist n (the last sublist might be shorter).
    If the given list has less equal than n items, a list of list is still returned
    """
    result = []
    for i in range(0, len(list_in), chunk_len):
        result.append(list_in[i:i + chunk_len])
    return result


def get_playlist_by_id(spotipy_client, playlist_id):
    playlist = spotipy_client.playlist(playlist_id)
    extend_playlist_tracks(spotipy_client, playlist)
    return playlist


def read_yaml(path):
    yaml_file = open(path, 'r')
    return yaml.load(yaml_file, yaml.SafeLoader)


def write_yaml(path, yaml_content):
    Path(path.parent).mkdir(parents=True, exist_ok=True)
    file = open(path, 'w')
    yaml.dump(yaml_content, file, sort_keys=False)
    file.close()
