from cerberus import Validator
from loguru import logger
from platformdirs import user_data_path
from src.utils import helpers
from pathlib import Path

alias_schema = {
    'settings': {
        'type': 'dict',
        'schema': {
            'client_id': {'type': 'string', 'required': True, 'minlength': 32, 'maxlength': 32},
            'client_secret': {'type': 'string', 'required': True, 'minlength': 32, 'maxlength': 32}
        }
    },
    'alias': {
        'type': 'dict'
    },
    'merge': {
        'type': 'dict'
    }
}


merge_schema = {
    'settings': {
        'type': 'dict',
        'schema': {
            'client_id': {'type': 'string', 'required': True, 'minlength': 32, 'maxlength': 32},
            'client_secret': {'type': 'string', 'required': True, 'minlength': 32, 'maxlength': 32}
        }
    },
    'alias': {'type': 'dict', 'required': True},
    'merge': {'type': 'dict', 'required': True},
    'config_path': {
    }
}


def validate_config_alias(config):
    validator = Validator(alias_schema, allow_unknown=True)
    validator.validate(config)
    return validator


def validate_config_merge(config):
    validator = Validator(merge_schema, allow_unknown=True)
    validator.validate(config)
    return validator


def get_config(config_path):
    if not config_path:
        config_path = get_default_config_path()
    config_path = Path(config_path).absolute()
    logger.info('Reading in config from: ' + str(config_path))
    config = read_config(config_path)
    return config


def read_config(config_path):
    """Returns the Configuration object from the passed path"""
    config = helpers.read_yaml(config_path)
    config['config_path'] = config_path
    return config


def save_config(config):
    config_path = config['config_path']
    config.pop('config_path')
    helpers.write_yaml(config_path, config)
    logger.info('Saved config to: ' + str(config_path))


def get_default_config_path():
    path_directory = user_data_path('SpotifyPM', appauthor=False, roaming=True)
    path = path_directory / 'config.yaml'
    return path


def get_empty_config():
    return {
        'settings': {
            'client_id': '',
            'client_secret': ''
        },
        'alias': {},
        'merge': {}
    }