from loguru import logger
from spotipy import SpotifyOauthError

from src.utils import configuration, helpers


def alias(path):
    try:
        config = configuration.get_config(path)
    except FileNotFoundError as e:
        return logger.error('Couldn\'t find config. ' + str(e))
    validator = configuration.validate_config_alias(config)
    if validator.errors:
        return logger.error('Validation failed: ' + str(validator.errors))
    try:
        spotipy_client = helpers.spotipy_authenticate(config)
    except SpotifyOauthError:
        return logger.error('Authentication Error. Check your Spotify client ID and client secret')
    add_alias_section(spotipy_client, config)
    configuration.save_config(config)


def add_alias_section(spotipy_client, config):
    """
    Add aliases to the config and write to config.yaml
    """
    logger.info('Adding alias section to config')
    user_id = spotipy_client.me()['id']
    user_playlists = helpers.get_all_user_playlists(spotipy_client, user_id)
    if 'alias' not in config:
        config['alias'] = {}
    if not config['alias']:
        config['alias'] = {}
    for playlist in user_playlists:
        playlist_name = playlist['name']
        playlist_url = playlist['external_urls']['spotify']
        config['alias'][playlist_name] = playlist_url

