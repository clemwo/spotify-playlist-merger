import click

from src.commands import alias
from src.utils import configuration


def generate_config(path):
    config = configuration.get_empty_config()
    config_path = path
    if not config_path:
        config_path = configuration.get_default_config_path()
    config['config_path'] = config_path
    config['settings']['client_id'] = input('Enter Spotify client id: ')
    config['settings']['client_secret'] = input('Enter Spotify client secret: ')
    # TODO: save config logs after click asks for confirmation. looks stupid in command line
    configuration.save_config(config)
    if click.confirm('Do you want to generate an alias section now?'):
        alias.alias(config_path)
