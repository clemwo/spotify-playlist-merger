from loguru import logger
from spotipy import SpotifyOauthError

from src.utils import configuration, helpers


def merge(path):
    """
    Merges the Spotify Playlists as specified in the passed config file
    """
    try:
        config = configuration.get_config(path)
    except FileNotFoundError as e:
        return logger.error('Couldn\'t find config. ' + str(e))
    validator = configuration.validate_config_merge(config)
    if validator.errors:
        return logger.error('Validation failed: ' + str(validator.errors))
    try:
        spotipy_client = helpers.spotipy_authenticate(config)
    except SpotifyOauthError:
        return logger.error('Authentication Error. Check your Spotify client ID and client secret')
    merge_playlists(spotipy_client, config)


def merge_playlists(spotipy_client, config):
    """
    Merges the playlists in the given config
    """
    merge_list = generate_merge_list(config)
    for target_playlist_id in merge_list:
        target_playlist = helpers.get_playlist_by_id(spotipy_client, target_playlist_id)
        # iterate through the source playlists of every target playlist
        for source_playlist_id in merge_list.get(target_playlist_id):
            source_playlist = helpers.get_playlist_by_id(spotipy_client, source_playlist_id)
            # spotify API is limited to 100 tracks, for longer playlists API has to be called multiple times
            helpers.extend_playlist_tracks(spotipy_client, target_playlist)
            merge_source_target_playlist(spotipy_client, target_playlist, source_playlist)
            helpers.extend_playlist_tracks(spotipy_client, source_playlist)


def generate_merge_list(config):
    """
    Reads in the config object and returns a list of playlists to merge in which the aliases are replaced by
    the URIs of the playlists
    """
    # create result data dictionary
    merge_list = {}
    for target_playlist in config['merge']:
        # get url of target playlist
        target_playlist_url = config['alias'][target_playlist]
        # create list as value for key which is target playlist url
        merge_list[target_playlist_url] = []
        # add source playlist urls to aforementioned list
        for source_playlist in config['merge'][target_playlist]:
            merge_list[target_playlist_url].append(config['alias'][source_playlist])
    return merge_list


def merge_source_target_playlist(sp, target_playlist, source_playlist):
    """
    Adds all tracks from the source playlist to the destination playlist
    """
    source_name = source_playlist['name']
    target_name = target_playlist['name']
    # get track ids from source and target playlist
    source_playlist_track_ids = []
    for item in source_playlist['tracks']['items']:
        if not item['is_local']:
            source_playlist_track_ids.append(item['track']['uri'])
    target_playlist_track_ids = []
    for item in target_playlist['tracks']['items']:
        if not item['is_local']:
            target_playlist_track_ids.append(item['track']['uri'])

    # get all the track ids that are included in source but not in target
    tracks_to_add = [track_id for track_id in source_playlist_track_ids if track_id not in target_playlist_track_ids]
    if len(tracks_to_add) == 0:
        logger.opt(ansi=True).info(
            'No tracks in "<green>' + source_name + '</green>" that aren\'t already in "<green>' + target_name + '</green>"')
    else:
        logger.info(
            'Merging ' + str(len(tracks_to_add)) + ' tracks from "' + source_name + '" to "' + target_name + '"')
        for tracks_to_add_chunk in helpers.divide_chunks(tracks_to_add, 100):
            sp.playlist_add_items(target_playlist['uri'], tracks_to_add_chunk)