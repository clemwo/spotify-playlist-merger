import click
from src.commands import alias, merge, generate_config


@click.group()
def cli():
    pass


@cli.command(name='alias')
@click.option('-p', '--path', help='Path to the config file')
def alias_command(path):
    """
    Generates a .yaml file with pairs of names and URIs of all Spotify playlists
    """
    alias.alias(path)


@cli.command(name='merge')
@click.option('-p', '--path', help='Path to the config file')
def merge_command(path):
    """
    Merges the Spotify Playlists as specified in the passed config file
    """
    merge.merge(path)


@cli.command(name='generate-config')
@click.option('-p', '--path', help='Path to the config file')
def generate_config_command(path):
    """
    Generates a config file in path. If path is not specified generate config in default location.
    """
    generate_config.generate_config(path)


if __name__ == '__main__':
    cli()

